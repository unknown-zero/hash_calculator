import sys
import os
import hashlib

def create_file_hash_info(file_name):
    f = open(file_name, 'rb')
    data = f.read()
    f.close()

    MD5 = hashlib.md5(data).hexdigest()
    SHA1 = hashlib.sha1(data).hexdigest()
    SHA256 = hashlib.sha256(data).hexdigest()
    SIZE = str(os.path.getsize(sys.argv[1])) + ' Bytes'

    return MD5, SHA1, SHA256, SIZE

if len(sys.argv) != 2:
    print("usage: hash_cal (file or directory)")
    sys.exit(1)

file_name = sys.argv[1]

if os.path.isfile(file_name):
    MD5, SHA1, SHA256, SIZE = create_file_hash_info(file_name)
    f = open(file_name + '_hash_result.txt', 'w')
    f.write('File Name: ')
    f.write(file_name)
    f.write('\n\n')

    f.write('Hash Result\n\n')

    f.write('MD5: ')
    f.write(MD5)
    f.write('\n')
    f.write('SHA-1: ')
    f.write(SHA1)
    f.write('\n')
    f.write('SHA-256: ')
    f.write(SHA256)
    f.write('\n')
    f.write('SIZE: ')
    f.write(SIZE)
    f.write('\n\n')

    f.write('This hash result was calculated by hash_cal[https://bitbucket.org/unknown-zero/hash_calculator/src]')

    f.close()
    
else:
    print("{0} not Found".format(file_name))
    sys.exit(2)

